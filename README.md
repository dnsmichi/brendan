# Hi, I'm Brendan 👨‍💻

I'm a technologist, a DevOps fanatic, and speaker.  I currently work as a Staff Developer Evangelist for GitLab.

## Find me on the internet
- [Twitter](https://twitter.com/olearycrew)
- [Web](https://boleary.dev)
